import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  divide(dividendo, divisor): number | string{
        if(divisor === 0){
          return 'Não existe divisão por Zero!';
        }
        return dividendo / divisor;
  }
  soma(soma1, soma2): number | string{
    return soma1 + soma2;
  }

  multiplica(mult1, mult2): number | string{
    return mult1 * mult2;
  }

  subtrai(sub1, sub2): number | string{
    return sub1 - sub2;
  }
}
