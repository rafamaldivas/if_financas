import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { CalculadoraPage } from './calculadora.page';

describe('A página Calculadora', () => {
  let view;
  let model: CalculadoraPage;
  let fixture: ComponentFixture<CalculadoraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculadoraPage ],
      imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CalculadoraPage);
    model = fixture.componentInstance;
    view = fixture.nativeElement;
    fixture.detectChanges();
  }));

  it('deve ter um título', () =>{
      expect(model.titulo).toBeDefined();
  });

  it('deve renderizar o título', () =>{
      const result = view.querySelector('.title').textContent;
      expect(result).toEqual('Calculadora');
  });

  it('deve estar com o botão desabilitado', () =>{
      expect(model.form.invalid).toBeTrue();
  });

  it('divide com clique no botão', () =>{
    // arrange
    model.form.controls.divisor.setValue(4);
    model.form.controls.dividendo.setValue(20);

    // act
    const operacao = fixture.debugElement.query(By.css('#operacao'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    // assert
    const quociente = view.querySelector('#quociente');
    expect(quociente.textContent).toEqual('5');
  })

  it('multiplica com clique no botão', () =>{
    // arrange
    model.form.controls.multi1.setValue(5);
    model.form.controls.multi2.setValue(10);

    // act
    const operacao = fixture.debugElement.query(By.css('#operacao'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    // assert
    const multiplicacao = view.querySelector('#multiplicacao');
    expect(multiplicacao.textContent).toEqual('50');
  })

  it('soma com clique no botão', () =>{
    // arrange
    model.form.controls.soma1.setValue(4);
    model.form.controls.soma2.setValue(8);

    // act
    const operacao = fixture.debugElement.query(By.css('#operacao'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    // assert
    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('12');
  })

  it('subtrai com clique no botão', () =>{
    // arrange
    model.form.controls.sub1.setValue(4);
    model.form.controls.sub2.setValue(20);

    // act
    const operacao = fixture.debugElement.query(By.css('#operacao'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    // assert
    const subtracao = view.querySelector('#subtracao');
    expect(subtracao.textContent).toEqual('-16');
  })
});
